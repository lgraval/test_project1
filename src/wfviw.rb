require "sinatra"
require "sequel"
require "sequel/extensions/core_extensions" # for lit()
require "json"

DB = Sequel.connect(ARGV.shift || "sqlite://deployments.db")
DB.create_table? :environments do
  String :name, :null => false, :unique => true
  primary_key :id
end

DB.create_table? :deployments do
  String :name, :null => false, :index => true
  String :version, :null => false
  String :hostname
  String :deployed_by
  Time   :deployed_at, :null => false, :default => "datetime('now', 'localtime')".lit

  primary_key :id
  foreign_key :environment_id, :environments, :null => false
end

class Deployment < Sequel::Model
  many_to_one :environment

  dataset_module do
    def latest
      eager(:environment).order(:environment_id, :name).group_by(:environment_id, :name).having { max(id) }
    end
  end
end

class Environment < Sequel::Model
  one_to_many :deployments
end

class DeployManager
  class << self
    def latest(q = {})
      env = q["env"].to_i
      sort = {:dir => q["sort_dir"],
              :col => q["sort_col"]}

      rs = Deployment.latest
      rs = rs.where(:environment_id => env) if env > 0

      # Sorting defaults to name column in ascending order if empty or invalid value
      sort[:col] = 'name' unless ['name', 'deployed_at'].include?(sort[:col])
      sort[:dir] = 'asc' unless ['asc', 'desc'].include?(sort[:dir])
      rs = rs.order(Sequel.send(sort[:dir], sort[:col].to_sym))
      list(rs, q)
    end

    def list(rs, q = {})
      q = q.dup
      page = q.delete('page').to_i
      page = 1  unless page > 0

      size = q.delete('size').to_i
      size = 10 unless size > 0

      # order...
      rs.limit(size, size * (page - 1))
    end

    def count
      Deployment.latest.all.count
    end

    def delete(id)
      Deployment.where(:id => id).delete
    end

    def create(attrs)
      attrs = attrs.dup
      Deployment.db.transaction do
        env = Environment.find_or_create(:name => attrs.delete("environment"))
        Deployment.create(attrs.merge(:environment => env))
      end
    end

    def environments
      Environment.all
    end
  end
end

helpers do
  def h(text)
    Rack::Utils.escape_html(text)
  end

  def sorting_column(column, text, params)
    sort = {:dir => params["sort_dir"],
            :col => params["sort_col"]}
    sort[:col] = 'name' unless ['name', 'deployed_at'].include?(sort[:col])
    sort[:dir] = 'asc' unless ['asc', 'desc'].include?(sort[:dir])

    action = (sort[:col].to_sym == column ? "sorted #{sort[:dir]}" : '')

    html = "<th class='#{action}'>" +
      text +
      "<div class='sorting'>" +
      "<a class='asc' href='/?sort_col=#{column}&sort_dir=asc'>asc</a>" +
      "<a class='desc' href='/?sort_col=#{column}&sort_dir=desc'>desc</a>" +
      "</div>" +
      "</th>"
  end

  def pagination(params)
    pages = ''
    current_page = params['page'] || 1
    current_page = current_page.to_i

    size = params['size'] || 10
    count = DeployManager.count

    prev_page = page_url('prev', current_page, size, count, params)
    pages << "<li><a href='#{prev_page}'>&laquo;</a></li>" unless prev_page.nil?

    (current_page-2..current_page+2).to_a.each do |page|
      next unless page > 0
      next if (page - 1) * size > count
      if current_page == page
        pages << "<li class='disabled'><a href='#'>#{page}</a></li>"
      else
        pages << "<li><a href='#{page_url(page, current_page, size, count, params)}'>#{page.to_s}</a></li>"
      end
    end

    next_page = page_url('next', current_page, size, count, params)
    pages << "<li><a href='#{next_page}'>&raquo;</a></li>" unless next_page.nil?

    pages
  end

  def page_url(page, current_page, size, count, params)
    p = params.dup

    number = page
    if page == 'prev'
      number = current_page - 1
      return nil unless number > 0
    elsif page == 'next'
      number = current_page + 1
      puts "number #{number}"
      return nil if (number - 1) * size > count
    end

    p['page'] = number

    "/?#{p.collect{|k,v| "#{k}=#{v}"}.join('&')}"
  end
end

post "/deploy/:id/delete" do
  DeployManager.delete(params[:id])
  #redirect to("/")
  200
end

post "/deploy" do
  do_redirect = params.delete('redirect')
  DeployManager.create(params)
  redirect to("/") if do_redirect
  201
end

get "/" do
  @deploys      = DeployManager.latest(params)
  @environments = DeployManager.environments
  erb :index
end
